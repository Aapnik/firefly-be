from django.db import models
from django.contrib.auth.models import User
import datetime as datetime


class Profile(models.Model):
	user 		= models.OneToOneField(User,default=None,null=True,blank=True,on_delete=models.CASCADE)
	phone 		= models.CharField(max_length=15,null=False,blank=False)
	otp 		= models.CharField(max_length=4)
	created_at = models.DateTimeField(default=datetime.datetime.now, blank=False)
	Verified	= models.BooleanField(default=False)
	isupdated 	= models.BooleanField(default=False)

	def __str__(self):
		return self.phone

