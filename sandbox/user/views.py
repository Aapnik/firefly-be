from rest_framework import status
from django.contrib.auth.models import User
from django.contrib import auth
import random, string
import datetime
from datetime import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .validators import *
from shop.models import *
from oscar.apps.address.models import *
from useraddress.models import *
from django.contrib.auth import authenticate

class SendingOtp(APIView):
	authentication_classes=()
	permission_classes=()

	def post(self,request):
		print(request.data)
		mobile=request.data.get('phone_number',False)
		print(mobile)
		if mobile:
			if validate_mobile(mobile)==False:
				return Response({'message':'Invalid phonenumber'})
			user=User.objects.filter(username__iexact=mobile)
			mobile=str(mobile)
			otp=self.send_otp(mobile)
			otp=str(otp)
			print(otp)
			Now=datetime.datetime.now(timezone.utc)
			if Profile.objects.filter(phone=mobile).exists():
				profile=Profile.objects.filter(phone=mobile).update(otp=otp,created_at=Now)
			else:
				profile=Profile.objects.create(phone=mobile,otp=otp,created_at=Now,Verified=False)
				profile.save()
			return Response({'message': 'Otp Sent Sucessfully','otp':otp},  status=status.HTTP_200_OK)
		else:
			return Response({'message': 'phone_number is required'},  status=status.HTTP_400_BAD_REQUEST)


	def send_otp(self,mobile):
		key=random.randint(1000,9999)
		# print(str(key)+"this is key")
		if mobile:
			n=datetime.datetime.now(timezone.utc)
			old=Profile.objects.filter(phone=mobile)			
			if old.exists():
				old=old.last()
				old_key=old.otp
				c=n-old.created_at
				# print(c)
				timetook=divmod(c.days * 86400 + c.seconds, 60)
				if int(timetook[0])<15:
					# print(timetook[0])
					# print("this is old key")
					return old_key
				else:
					return key
			else:
				return key			 



class VerifyOTP(APIView):
	authentication_classes=()
	permission_classes=()

	def post(self,request):
		phone=request.data.get('phone_number',False)
		otp_sent=request.data.get('otp',False)
		print(phone)
		print(otp_sent)
		if phone and otp_sent:
			if validate_mobile(phone)==False:
				return Response({'message':'Invalid phonenumber'})

			old=Profile.objects.filter(phone=phone)
			if old.exists():
				old=old.last()
				otp=old.otp
				# print(otp)
				# print(otp_sent)
				if str(otp_sent)==str(otp):
					old.Verified=True
					old.save()
					try:
						# print('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
						user=User.objects.get(username=phone)
						details=Profile.objects.get(user=user)
					except:
						return Response({'message': 'Otp Verified',"Basicinfo":False,"StoreInfo":False,"Uploaddata":False,"isactive":False},  status=status.HTTP_400_BAD_REQUEST)
					if details.isupdated == True and user.is_active==True:
						user.set_password(otp)
						user.save()
						user = authenticate(username=phone, password=otp)
						print(user)
						auth.login(request, user)
						return Response({'message': 'Otp Verified',"Basicinfo":True,"StoreInfo":True,"Uploaddata":True,"isactive":True},  status=status.HTTP_200_OK)
					elif details.isupdated == True and user.is_active==False:
						return Response({'message': 'Otp Verified',"Basicinfo":True,"StoreInfo":True,"Uploaddata":True,"isactive":False},  status=status.HTTP_200_OK)
					elif details.isupdated == False and user.is_active==False:

						if UserAddress.objects.filter(user=user).exists()==True and Store.objects.filter(user=user,Customer_Id_Name='NULL',Store_Id_Name='NULL').exists()==True:
							return Response({'message': 'Otp Verified',"Basicinfo":True,"StoreInfo":True,"Uploaddata":False,"isactive":False},  status=status.HTTP_200_OK)
						if UserAddress.objects.filter(user=user).exists()==True and Store.objects.filter(user=user,Customer_Id_Name='NULL',Store_Id_Name='NULL').exists()==False:
							return Response({'message': 'Otp Verified',"Basicinfo":True,"StoreInfo":True,"Uploaddata":False,"isactive":False},  status=status.HTTP_200_OK)
				else:
					return Response({'message':"Incorrect otp"},status=status.HTTP_400_BAD_REQUEST)
			else:
				return Response({'message': 'Send Otp again User Doesnot exists'},  status=status.HTTP_400_BAD_REQUEST)
		else:
			return Response({'message': 'mobile and otp required'},  status=status.HTTP_400_BAD_REQUEST)




class RegisterBasicInfo(APIView):
	authentication_classes=()
	permission_classes=()
	def post(self,request):
		username=request.data.get('username',False)
		Fullname=request.data.get('fullname',False)
		email=request.data.get('email','')
		if username and Fullname:
			LastProfile=Profile.objects.filter(phone=username)
			users=User.objects.filter(username=username)
			if User.objects.filter(username=username).exists():
				return Response({"message":'Info Is Already Filled'},status=status.HTTP_400_BAD_REQUEST)
			if LastProfile.exists():
				LastProfile=LastProfile.last()
				if LastProfile.Verified == False:
					return Response({"message":'Please Verify Otp First'},status=status.HTTP_400_BAD_REQUEST)
				elif LastProfile.isupdated == True and users.is_active == True:
					return Response({"message":'Basic Info is Complete And You are Activated',"Basicinfo":True,"Uploaddata":True,"StoreInfo":True,"isactive":True})
				elif LastProfile.isupdated == True and users.is_active == False:
					return Response({"message":'You are Not Activated Our team will Contact you',"Basicinfo":True,"StoreInfo":True,"Uploaddata":True,"isactive":False})
				else:
					passw=self.GeneratePassword()
					print(passw)
					try:						
						user=User.objects.create(username=username,password=passw,first_name=Fullname.split(' ')[0],last_name=Fullname.split(' ')[1],email=email)
					except:
						user=User.objects.create(username=username,password=passw,first_name=Fullname.split(' ')[0],last_name=' ',email=email)
					
					print(user)
					user.is_active=False	
					user.save()
					LastProfile.user=User.objects.get(username=username)
					LastProfile.save()

					return Response({"message":"Details Saved","Basicinfo":True,"StoreInfo":False,"isactive":False,"Uploaddata":False},status=status.HTTP_200_OK)					
			else:
				return Response({"message":"First Send Otp And Verify !"})
		else:
			return Response({'message': 'Fullname  and Username cannot be empty'},  status=status.HTTP_400_BAD_REQUEST)

	def GeneratePassword(self):
		x = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
		return x

