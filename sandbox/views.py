from django.http import JsonResponse
from oscar.core.loading import get_model
from rest_framework import generics
from rest_framework.response import Response
from oscarapi.views import product
from serializers import (
    MyCategorySerializer,
    MyProductLinkSerializer,
    SubCategorySerializer1,
    ParentProductSerializer
)

Product = get_model('catalogue', 'Product')
Category = get_model('catalogue', 'category')


class ProductList(product.ProductList):
    serializer_class = ParentProductSerializer
    queryset = Product.objects.all()

    def get_queryset(self):
        qs = super(ProductList, self).get_queryset()
        categoryid = self.request.query_params.get('id')
        category = Category.objects.get(pk=categoryid)
        print(Product.objects.filter(categories=category))
        if category.get_children().exists() == False:
            qs = Product.objects.filter(categories=category)
        else:
            qs = qs.filter(pk=0)
            for sub_categories in category.get_children():
                qs |= Product.objects.filter(categories=sub_categories)
        return qs


'''Updated by gautam FIR-7 [Backend] - Code refactoring for Category API'''


class CategoryList(generics.ListAPIView):
    queryset = Category.objects.filter()
    serializer_class = MyCategorySerializer
    data2 = serializer_class.data

    def list(self, request, *args, **kwargs):
        response = super(CategoryList, self).list(request, *args, **kwargs)
        response.data = {"categories": response.data}
        return response

    def get(self, request, *args, **kwargs):
        level = self.request.query_params.get('level', None)

        print(level)
        if level is None:
            return JsonResponse({"error": "level is mandatory"}, status=400, safe=False)
        if level:
            if int(level) > 0:
                name = self.request.query_params.get('name', None)
                print(name)
                if name is None:
                    return JsonResponse(
                        {"error": "name is mandatory for level > 0"}, status=400, safe=False)

        data = list()

        queryset = self.get_queryset()

        for cat in queryset:
            categorylist = list()
            subcategorylist = list()
            categorylist.append({"id": cat.id, "name": cat.name,
                                 "image": cat.image.name, "path": cat.path, "sub_categories": ""})

            for subcat in cat.get_children():
                subcategorylist.append({"id": subcat.id, "name": subcat.name,
                                        "image": subcat.image.name, "path": subcat.path})
            # if len(subcategorylist) > 0:
            categorylist[0]["sub_categories"] = subcategorylist

            data.append({"categories": categorylist})

        # data = {'categories': list(queryset.values())}

        return JsonResponse(data, status=200, safe=False)

    def get_queryset(self):
        level = self.request.query_params.get('level')
        level = int(level)
        name = self.request.query_params.get('name', None)
        print(name)
        qs = super(CategoryList, self).get_queryset()
        if level > 0:
            qs = qs.filter(depth__in=[level + 1], name__iexact=name)
        if level == 0 and name is None:
            qs = qs.filter(depth__in=[level, level + 1])
        if level == 0 and name:
            qs = qs.filter(depth__in=[level, level + 1], name__iexact=name)

        # return {"a": qs }
        # print(type(qs))
        # print(qs.values())
        # return Response({'categories':qs})
        return qs


class CategoryDetail(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = SubCategorySerializer1


def get_json_list(query_set):
    list_objects = []
    for obj in query_set:
        dict_obj = {}
        for field in obj._meta.get_fields():
            try:
                if field.many_to_many:
                    dict_obj[field.name] = get_json_list(getattr(obj, field.name).all())
                    continue
                dict_obj[field.name] = getattr(obj, field.name)
            except AttributeError:
                continue
        list_objects.append(dict_obj)
    return list_objects
