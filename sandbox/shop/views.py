from rest_framework import generics
from oscar.apps.address.models import *
from rest_framework import status
from django.contrib.auth.models import User
import random
import datetime
from user.models import *
from datetime import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from Dropdown.models import *

class DocumentsubmissionView(APIView):
	authentication_classes=()
	permission_classes=()

	def get(self,request):
		Customer=[]
		Store_id=[]
		Custom=Dropdown.objects.filter(used_in_flow=Docname.objects.get(name='Costumer_docs'))
		i=0
		while(i<len(Custom)):
			Customer.append(Custom[i].Id.Id_name)
			i=i+1
		print(Customer)
		
		Store=Dropdown.objects.filter(used_in_flow=Docname.objects.get(name='Store_docs'))
		i=0
		while(i<len(Store)):
			Store_id.append(Store[i].Id.Id_name)
			i=i+1
		
		return Response({"Customer_doc":Customer,"Store_docs":Store_id})




	# def get(self,request):
	# 	Dropdown.objects.filter(Docname=)

	def post(self, request):
		username=request.data.get('username',False)
		store_address1=request.data.get('store_address1',False)
		store_address2=request.data.get('store_address2',False)
		costumer_id_proof=request.data.get('costumer_id_proof',False)
		costumer_id_name=request.data.get('costumer_id_name',False)
		store_id_proof=request.data.get('store_id_proof',False) 
		store_id_name=request.data.get('store_id_name',False) 
		print(store_address1)
		print(store_address2)
		if username and store_id_name and store_id_proof and costumer_id_name and costumer_id_proof and store_address1 and store_address2 :
			LastProfile=Profile.objects.filter(phone=username)
			if LastProfile.exists():
				LastProfile=LastProfile.last()
				if LastProfile.Verified == True:
					user=User.objects.get(username=username)
					Store_count=Store.objects.filter(user=user,StoreAddress1=store_address1,StoreAddress2=store_address2)
					if len(Store_count)!=1:
						return Response({'message':'Store with that address Doesnt/Already Exists',"Basicinfo":True,"Uploaddata":False,"StoreInfo":True,"isactive":False}, status=status.HTTP_400_BAD_REQUEST)

					Store_count.update(user=user,Customer_Id_Name=costumer_id_name,Customer_ID_Upload=costumer_id_proof,Store_Id_Name=store_id_name,Store_Id_Upload=store_id_proof)
					# Store.objects.create()
					LastProfile.isupdated=True
					LastProfile.save()
					return Response({'message':'Documents uploaded',"Basicinfo":True,"Uploaddata":True,"StoreInfo":True,"isactive":False}, status=status.HTTP_201_CREATED)
				else:
					return Response({'message':'User is Not Verified',"Basicinfo":False,"Uploaddata":False,"StoreInfo":False,"isactive":False}, status=status.HTTP_400_BAD_REQUEST)

			else:
				return Response({'message':'User Doesnt Exists Send OTP Again ',"Basicinfo":False,"Uploaddata":False,"StoreInfo":False,"isactive":False}, status=status.HTTP_400_BAD_REQUEST)
		else:
			return Response({'message':'Please Upload Documents',"Basicinfo":True,"Uploaddata":False,"StoreInfo":True,"isactive":False}, status=status.HTTP_400_BAD_REQUEST)


