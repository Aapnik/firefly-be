from django.db import models
from django.contrib.auth.models import User
from django.utils.functional import lazy

def upload_update_image(instance,filename):
    return "ShopDocuments/Idproof/{filename}".format(user=instance.user.username,filename=filename)

def user_directory_path(instance, filename):
	return "ShopDocuments/Documents/{filename}".format(user=instance.user.username,filename=filename)


class Store(models.Model):	
	user 		= models.ForeignKey(User,on_delete=models.CASCADE)
	Storename 	= models.CharField(max_length=100)
	StoreAddress1 = models.CharField(max_length=1000)
	StoreAddress2 = models.CharField(max_length=1000)
	Pannumber 	= models.CharField(max_length=20)
	GstNumber 	= models.CharField(max_length=20)
	Customer_Id_Name=models.CharField(max_length=100)
	Customer_ID_Upload 	= models.FileField(upload_to=upload_update_image)
	Store_Id_Name = models.CharField(max_length=100)
	Store_Id_Upload	= models.FileField(upload_to=user_directory_path)

	def __str__(self):
		return self.user.username





