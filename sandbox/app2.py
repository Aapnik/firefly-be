from django.conf.urls import url

from oscarapi.app import RESTApiApplication
import views

from carouselpromotions.views import CarouselList

from user.views import *
from shop.views import *
from useraddress.views import *



class MyRESTApiApplication(RESTApiApplication):

    def get_urls(self):
        urls = [

            url(
                r'^categories/$',

                views.CategoryList.as_view(), name='categories'),
            url(
                r'^carousel/$',
                CarouselList.as_view(), name='carousel'),
            url(
                r'^products/$',
                views.ProductList.as_view(), name='products'),
            
            url(r'^sendotp/$', SendingOtp.as_view(),name='sendotp'),

            url(r'^verifyotp/$', VerifyOTP.as_view(),name='verifyotp'),

            url(r'^basicinfo/$', RegisterBasicInfo.as_view(),name='basicinfo'),

            url(r'^storeinfo/$', RegisterStoreInfo.as_view(),name='storeinfo'),

            url(r'^documents/$', DocumentsubmissionView.as_view(),name='documents'),

        ]

        return urls + super(MyRESTApiApplication, self).get_urls()


application = MyRESTApiApplication()
