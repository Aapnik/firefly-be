from django.conf import settings
from oscar.core.loading import get_class, get_model
from rest_framework import serializers

from oscarapi.serializers import checkout, product
from oscarapi.utils import (
    OscarModelSerializer
)

Category = get_model('catalogue', 'category')

Product = get_model('catalogue', 'Product')

Selector = get_class('partner.strategy', 'Selector')



class MyProductLinkSerializer(product.ProductLinkSerializer):
    img_url = serializers.SerializerMethodField()

    price = serializers.SerializerMethodField()

    class Meta(product.ProductLinkSerializer.Meta):
        fields = ('id', 'title', 'images', 'price','structure',"attributes")

    def get_price(self, obj):
        request = self.context.get("request")
        strategy = Selector().strategy(
            request=request, user=request.user)

        ser = checkout.PriceSerializer(
            strategy.fetch_for_product(obj).price,
            context={'request': request})
        if strategy.fetch_for_product(obj).stockrecord is not None:
            price_retail=strategy.fetch_for_product(obj).stockrecord.price_retail
        else:
            price_retail=0

        if ser.data['excl_tax']!=None and ser.data['incl_tax']!=None and ser.data['tax']!=None:
            return {"currency":ser.data['currency'],"mrp":float(price_retail),"excl_tax":float(ser.data['excl_tax']),"incl_tax":float(ser.data['incl_tax']),"tax":float(ser.data['tax'])}
        else:
            return {"currency":ser.data['currency'],"mrp":float(price_retail),"excl_tax":ser.data['excl_tax'],"incl_tax":ser.data['incl_tax'],"tax":ser.data['tax']} 



    # def get_img_url(self, obj):
    #     if not obj.images:
    #         return 
    #     return obj.images #@cbns

class ParentProductSerializer(product.ProductLinkSerializer):
    products = MyProductLinkSerializer(
        many=True, source='children'
    )

    # price = serializers.SerializerMethodField()

    class Meta(product.ProductLinkSerializer.Meta):
        fields = ('id', 'title','structure', 'products',)


    # def get_price(self, obj):
    #     request = self.context.get("request")
    #     strategy = Selector().strategy(
    #         request=request, user=request.user)

    #     ser = checkout.PriceSerializer(
    #         strategy.fetch_for_product(obj).price,
    #         context={'request': request})

    #     return ser.data

    # def get_img_url(self, obj):
    # if not obj.image:
    # return ""
    # return obj.image #@cbns


class SubCategorySerializer(OscarModelSerializer):
    img_url = serializers.SerializerMethodField()

    class Meta():
        model = Category
        fields = ('id', 'name', 'img_url', 'url')

    def get_img_url(self, obj):
        if not obj.image:
            return ""
        return str(settings.MEDIA_URL) + str(obj.image)
        # return "hello" #@cbns


class MyCategorySerializer(OscarModelSerializer):
    img_url = serializers.SerializerMethodField()

    sub_categories = SubCategorySerializer(
        many=True, source='get_children'
    )

    class Meta():
        model = Category
        fields = ('id', 'name', 'img_url', 'url', 'sub_categories')

    def get_img_url(self, obj):
        if not obj.image:
            return ""
        return str(settings.MEDIA_URL) + str(obj.image)

        # return settings.MEDIA_ROOT+settings.MEDIA_URL +obj.image #@cbns

    # def to_representation(self, instance):
    # data = super(MyCategorySerializer, self).to_representation(instance)
    # data.update(...)
    # pass
    # print(data)

    # return { 'categories': data}


class SubCategoryWithProductSerializer(OscarModelSerializer):
    img_url = serializers.SerializerMethodField()

    products = MyProductLinkSerializer(
        many=True, source='product_set'
    )

    class Meta():
        model = Category
        fields = ('id', 'name', 'img_url', 'products')

    # def get_img_url(self, obj):
    # if not obj.image:
    # return ""
    # return obj.image  # @cbns





class SubCategorySerializer1(OscarModelSerializer):
    img_url = serializers.SerializerMethodField()

    sub_categories = ParentProductSerializer(
        many=True, source='product_set'
    )

    class Meta():
        model = Category
        fields = ('id', 'name', 'img_url', 'sub_categories')

    def get_img_url(self, obj):
        if not obj.image:
            return ""
        return str(settings.MEDIA_URL) + str(obj.image)
