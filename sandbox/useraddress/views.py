from oscar.core.loading import get_model
from oscar.apps.address.models import *
from rest_framework import status
from django.contrib.auth.models import User
import random
import datetime
from datetime import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from shop.models import *
from user.models import *

class RegisterStoreInfo(APIView):
	authentication_classes=()
	permission_classes=()
	def post(self,request):
		username=request.data.get('username',False)
		Storename=request.data.get('storename',False)
		Address1=request.data.get('address1',False)
		Address2=request.data.get('address2',False)
		Pincode=request.data.get('pincode',False)
		District=request.data.get('district',False)
		State=request.data.get('state',False)
		Pannumber=request.data.get('pannumber',False)
		GstNumber=request.data.get('gstnumber',False)
		# print(username)
		# print(Storename)
		# print(Address1)
		# print(Address2)
		# print(Pincode)
		# print(District)
		# print(State)
		# print(Pannumber)
		# print(GstNumber)
		if username and Storename and Address1 and Address2 and Pincode and District and State and Pannumber and GstNumber:
			Indiaphone='+91'+ str(username)
			try:
				user=User.objects.get(username=username)
			except:
				return Response({'message': 'User Does Not Exist ',"Basicinfo":False,"StoreInfo":False,"Uploaddata":False,"isactive":False},  status=status.HTTP_400_BAD_REQUEST)
			countr,created=Country.objects.get_or_create(iso_3166_1_a2='IN',name="India",printable_name='India',is_shipping_country=True,display_order=1)

			try:
				UserAddress.objects.create(user=user,first_name=user.first_name,last_name=user.last_name,line1=Address1,line2=Address2 ,line4=District,state=State,postcode=Pincode,country=countr,is_default_for_shipping=True,is_default_for_billing=True,phone_number=Indiaphone)
				Store.objects.create(user=user,Storename=Storename,Pannumber=Pannumber,GstNumber=GstNumber,StoreAddress1=Address1,StoreAddress2=Address2,Store_Id_Name='NULL',Customer_Id_Name='NULL')
			except:
				return Response({'message':'This address is already in the address book',"Basicinfo":True,"StoreInfo":False,"Uploaddata":False,"isactive":False})			

			profile=Profile.objects.filter(user=user)
			if profile.exists():
				profile=profile.first()
			else:
				return Response({'message':'Send otp andverofy',"Basicinfo":False,"StoreInfo":False,"Uploaddata":False,"isactive":False})


			if user.is_active==True and profile.isupdated==True:
				return Response({'message': 'Details Filled',"Basicinfo":True,"StoreInfo":True,"Uploaddata":True, "isactive":True },  status=status.HTTP_200_OK)
			elif user.is_active==False:
				if profile.isupdated==True:
					return Response({'message': 'Details Filled',"Basicinfo":True,"StoreInfo":True,"Uploaddata":True, "isactive":False },  status=status.HTTP_200_OK)
				elif profile.isupdated==False:
					return Response({'message': 'Details Filled',"Basicinfo":True,"StoreInfo":True,"Uploaddata":False, "isactive":False },  status=status.HTTP_200_OK)
				
			elif user.is_active==True:
				if profile.isupdated==True:
					return Response({'message': 'Details Filled',"Basicinfo":True,"StoreInfo":True,"Uploaddata":True, "isactive":True },  status=status.HTTP_200_OK)
				elif profile.isupdated==False:
					return Response({'message': 'Details Filled',"Basicinfo":True,"StoreInfo":True,"Uploaddata":False, "isactive":True },  status=status.HTTP_200_OK)
		else:
			return Response({'message': 'Details cannot be empty ',"Basicinfo":True,"StoreInfo":False,"Uploaddata":False,"isactive":False},  status=status.HTTP_400_BAD_REQUEST)



# class Accountinfo(APIView):
# 	authentication_classes=()
# 	permission_classes=()

# 	def get(self,request):
# 		print(request.user)
# 		phone=request.query_params.get('phone')
# 		if phone:
# 			profile=Profile.objects.filter(phone=phone)
# 			if len(profile)==0:				
# 				return Response({"message":"send otp and verify"},status=status.HTTP_400_BAD_REQUEST)
# 			else:
# 				details=profile.first()



# 			user=User.objects.filter(username=phone)
# 			if len(user)==0:
# 				return Response({"message":"user doesnt exists"},status=status.HTTP_200_OK)
# 			else:				
# 				USER=user.first()

# 			if details.isupdated == True and user.is_active==True:
# 				address=UserAddress.objects.filter(user=phone)
# 				return Response({"Address":address},status=status.HTTP_200_OK)
# 			else:
# 				return Response({'message':'user is not authenticated'},status=status.HTTP_400_BAD_REQUEST)
# 		else:
# 			return Response({'message':'phonenumber is required'},status=status.HTTP_400_BAD_REQUEST)













