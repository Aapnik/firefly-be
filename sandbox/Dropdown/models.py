from django.db import models


class Ids(models.Model):

	Id_name = models.CharField(max_length=50,unique=True)

	
	def __str__(self):
		return self.Id_name
	
class Docname(models.Model):
	name=models.CharField(max_length=100)

	def __str__(self):
		return self.name


class Dropdown(models.Model):
	used_in_flow = models.ForeignKey(Docname,max_length=100,on_delete=models.CASCADE)
	Id=models.ForeignKey(Ids,max_length=50,on_delete=models.CASCADE,default=None)

	def __str__(self):
		return (self.used_in_flow.name + " "+self.Id.Id_name) 
