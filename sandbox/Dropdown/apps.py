from django.apps import AppConfig


class DropdownConfig(AppConfig):
    name = 'Dropdown'
