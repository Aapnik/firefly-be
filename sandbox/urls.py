
from django.conf import settings

from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin
from oscar.app import application
from oscarapi.app import application as api
import os
print(os.getcwd())
from app2 import application as mycustomapi

from user.views import *
from useraddress.views import *
from shop.views import *
urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', api.urls),
    url(r'^customapi/', mycustomapi.urls),
    url(r'', application.urls),
     							       
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)