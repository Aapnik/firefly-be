from models import CarouselPromotion
from oscar.apps.promotions.conf import *


def get_promotion_classes():
    return (RawHTML, Image, CarouselPromotion, SingleProduct,
            AutomaticProductList, HandPickedProductList, MultiImage)


PROMOTION_CLASSES = get_promotion_classes()
