from rest_framework import serializers
from .models import CarouselPromotion


class MyCarouselSerializer(serializers.ModelSerializer):
    class Meta():
        model = CarouselPromotion
        fields = ('id', 'link_url', 'name', 'description', 'image', 'order',)
