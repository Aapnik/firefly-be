from django.contrib import admin
from .models import *
class CarouselAdmin(admin.ModelAdmin):
    list_display = ('name','description','date_created','order','active')

admin.site.register(CarouselPromotion,CarouselAdmin)
