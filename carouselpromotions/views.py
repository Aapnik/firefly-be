from django.http import JsonResponse
from rest_framework import generics

from .serializers import MyCarouselSerializer
from .models import CarouselPromotion


'''Created by gautam FIR-3 [Backend] - Code  for CarouselPromotion API'''


class CarouselList(generics.ListAPIView):
    queryset = CarouselPromotion.objects.filter()
    serializer_class = MyCarouselSerializer
    data2 = serializer_class.data

    def list(self, request, *args, **kwargs):
        response = super(CategoryList, self).list(request, *args, **kwargs)
        response.data = {"offers": response.data}
        return response

    def get(self, request, *args, **kwargs):
        data = list()
        queryset = self.get_queryset()
        data = {'offers': list(queryset.values())}
        return JsonResponse(data, status=200, safe=False)

    def get_queryset(self):
        qs = super(CarouselList, self).get_queryset()
        qs = qs.filter(active=True).order_by('-order')

        return qs
