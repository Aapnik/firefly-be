from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from oscar.apps.promotions.models import AbstractPromotion
from oscar.models.fields import ExtendedURLField


class CarouselPromotion(AbstractPromotion):
    _type = 'Carousel'
    name = models.CharField(_("Name"), max_length=128)
    description = models.TextField(_("Description"))
    link_url = ExtendedURLField(
        _('Link URL'), blank=True,
        help_text=_('This is where this promotion links to'))
    image = models.ImageField(
        _('Image'), upload_to=settings.OSCAR_PROMOTION_FOLDER,
        max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)
    order = models.IntegerField()
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("carousel")
        verbose_name_plural = _("carousel")
